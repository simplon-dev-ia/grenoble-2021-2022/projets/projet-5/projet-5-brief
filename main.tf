terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.6.0"
    }
  }

  backend "http" {
  }
}

variable "pg_admin_username" {
  type = string
  description = "Admin username for PostgreSQL database server"

  validation {
    condition = length(var.pg_admin_username) > 0
    error_message = "Admin username cannot start with numbers and must only contain characters and numbers"
  }
}

variable "pg_admin_password" {
  type = string
  description = "Admin password for PostgreSQL database server"

  validation {
    condition     = length(var.pg_admin_password) > 7 && length(var.pg_admin_password) < 129
    error_message = "Passwords must be at least 8 characters and at most 128 characters, and contain characters from three of the following categories - English uppercase letters, English lowercase letters, numbers (0-9), and non-alphanumeric characters (!, #, %, etc.)."
  }
}

locals {
  grenoble_ia1_p7 = {
    principal_id = "a0c131e3-e748-461f-89a4-b344a5ed68fd"
  }

  groupe1 = {
    mboulli = {
      principal_id = "abf6322e-4427-4cb9-b2bb-66b02369f24f"
    }
    tquach = {
      principal_id = "784d201f-dce3-4920-a541-780a356b9e68"
    }
    gcaferra = {
      principal_id = "beee6421-90a1-4daf-8e72-6db181182643"
    }
  }
}

provider "azurerm" {
  features {
    resource_group {
      prevent_deletion_if_contains_resources = false
    }
  }
}

data "azurerm_subscription" "subscription" {}

resource "azurerm_resource_group" "default" {
  name     = "p7-projet5"
  location = "France Central"
}

resource "azurerm_container_registry" "default" {
  name                = "p7Projet5Acr"
  resource_group_name = azurerm_resource_group.default.name
  location            = azurerm_resource_group.default.location
  sku                 = "Basic"
  admin_enabled       = true
}

resource "azurerm_service_plan" "default" {
  name                = "p7-projet5-asp"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  os_type             = "Linux"
  sku_name            = "F1"
}

resource "azurerm_linux_web_app" "default" {
  for_each = toset(["groupe1"])

  name                = "p7-projet5-webapp-${each.value}"
  location            = azurerm_resource_group.default.location
  resource_group_name = azurerm_resource_group.default.name
  service_plan_id     = azurerm_service_plan.default.id

  site_config {
    application_stack {
      docker_image     = "mcr.microsoft.com/appsvc/staticsite"
      docker_image_tag = "latest"
    }
    always_on          = false
    use_32_bit_worker  = true
    websockets_enabled = true
  }
}

resource "azurerm_postgresql_flexible_server" "default" {
  name                   = "p7-projet5-pg-server"
  location               = azurerm_resource_group.default.location
  resource_group_name    = azurerm_resource_group.default.name
  version                = "13"
  administrator_login    = var.pg_admin_username
  administrator_password = var.pg_admin_password
  storage_mb             = 32768
  sku_name               = "B_Standard_B1ms"
  zone                   = "1"
}

resource "azurerm_postgresql_flexible_server_database" "default" {
  for_each = toset(["groupe1"])

  name      = "p7-projet5-pg-db-${each.value}"
  server_id = azurerm_postgresql_flexible_server.default.id
  collation = "en_US.utf8"
  charset   = "utf8"
}

resource "azurerm_role_assignment" "acr_role" {
  for_each = local.groupe1

  scope                = azurerm_container_registry.default.id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_role_assignment" "lwa_role" {
  for_each = local.groupe1

  scope                = azurerm_linux_web_app.default["groupe1"].id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}

resource "azurerm_role_assignment" "pg_role" {
  for_each = local.groupe1

  scope                = azurerm_postgresql_flexible_server.default.id
  role_definition_name = "Reader"
  principal_id         = each.value.principal_id
}
